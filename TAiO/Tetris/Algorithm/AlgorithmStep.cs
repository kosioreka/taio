﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Algorithm
{
    public class AlgorithmStep
    {
        /// <summary>
        /// The well to which we want to add another block
        /// </summary>
        protected Well CurrentWell;

        /// <summary>
        /// List of quantities of available blocks. Index corresponds to the index in Blocks list
        /// </summary>
        protected int[] Quantities;

        /// <summary>
        /// List of blocks available
        /// </summary>
        protected List<Block> Blocks;

        /// <summary>
        /// List of k best wells after adding another block
        /// </summary>
        protected List<Well> BestWells;

        public AlgorithmStep(List<Block> blocks, int[] quantities, Well currentWell)
        {
            Blocks = blocks;
            Quantities = quantities;
            CurrentWell = currentWell;
            BestWells = new List<Well>();
        }
        /// <summary>
        /// Returns the best K wells created from the CurrentWell
        /// </summary>
        /// <param name="k">Max number of wells to return</param>
        /// <returns></returns>
        public List<Well> GetWells(int k)
        {

#if DISABLE_PARALLEL
            for (int i = 0; i < Blocks.Count; i++)
#else
            var sync = new object();
            Parallel.For(0, Blocks.Count, (i) =>
#endif
            {
                if (Quantities[i] > 0)
                {
                    for (int j = 0; j < Block.NumberOfRadiuses; j++)
                    {
                        Well w = Helper.DeepCopy(CurrentWell);
                        Block b = Helper.DeepCopy(Blocks[i]);
                        b.Radius = j;
                        if(w.AddBlock(b, i))
#if !DISABLE_PARALLEL
                            lock (sync)
#endif
                            BestWells.Add(w);
                    }
                }
            }
#if !DISABLE_PARALLEL
            );
#endif
            return Well.GetBestWells(BestWells, k);
        }
    }
}
