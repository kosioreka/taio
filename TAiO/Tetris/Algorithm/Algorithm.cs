﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Algorithm
{
    public class Algorithm
    {
        /// <summary>
        /// Number of wells to consider on each level
        /// </summary>
        public int K { get; set; } = 1;

        /// <summary>
        /// The width of a well
        /// </summary>
        protected int WellWidth { get; }

        /// <summary>
        /// List of best wells at the current algorithm step
        /// </summary>
        public List<Well> BestWells
        {
            get; set;
        }

        /// <summary>
        /// List of blocks available
        /// </summary>
        public List<Block> Blocks { get; }

        /// <summary>
        /// List of quantities of available blocks. Index corresponds to the index in Blocks list
        /// </summary>
        protected int[] Quantities;

        public Algorithm(List<Block> blocks, int[] quantities, int wellWidth)
        {
            Blocks = blocks;
            Quantities = quantities;
            WellWidth = wellWidth;
            BestWells = new List<Well>();
        }

        /// <summary>
        /// Number of steps to make
        /// </summary>
        /// <param name="steps"></param>
        public void Step(int steps = 1)
        {
            if (K != BestWells.Count)
            {
                AdjustWellsToK();
            }
            for (var i = 0; i < steps; i++)
            {
                var temp = new List<Well>();
                var tempSync =new object();
#if DISABLE_PARALLEL
                foreach (var w in BestWells)
#else
                Parallel.ForEach(BestWells, w =>
#endif
                {
                    var wells = new AlgorithmStep(Blocks, GetQuantities(w), w).GetWells(K);
                    lock (tempSync) temp.AddRange(wells);
#if DISABLE_PARALLEL
                }
#else
                });
#endif
                BestWells = Well.GetBestWells(temp, K);
            }
        }
        /// <summary>
        /// Keeps the number of wells accordingly to the k parameter
        /// </summary>
        protected void AdjustWellsToK()
        {

            BestWells.AddRange(Enumerable.Repeat(new Well(Blocks.Count, WellWidth), K - BestWells.Count));
        }

        /// <summary>
        /// Get block available quantities for the current well
        /// </summary>
        /// <param name="w">Well to get quantities for</param>
        /// <returns>Available block quantities</returns>
        protected int[] GetQuantities(Well w)
        {
            int[] temp = new int[Blocks.Count];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = Quantities[i] - w.QuantitiesUsed[i];
            }
            return temp;
        }
    }
}
