﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Parser
{
    /// <summary>Parser dla plików zadań</summary>
    public class TaskParser
    {
        const char Separator = ' ';
        /// <summary>
        /// Method to parse file
        /// </summary>
        /// <param name="filePath">The path to the file</param>
        /// <returns>Parsed task</returns>
        public TaioTask ParseFile(string filePath)
        {
            var lines = File.ReadAllLines(filePath);
            return ParseLines(lines);
        }
        /// <summary>
        /// Method to parse lines
        /// </summary>
        /// <param name="lines">Lines to parse</param>
        /// <returns>Teh task</returns>
        public TaioTask ParseLines(string[] lines)
        {
            //pobierz wymiar planszy i ilość klocków
            var numbers = lines[0].Split(Separator);
            Debug.Assert(numbers.Length == 2, "Błędny opis wymiaru studni i liczby klocków");//taki Debug overkill XD
            var wellWidth = int.Parse(numbers[0]);
            var blockCount = int.Parse(numbers[1]);
            var blocks = new List<Block>(blockCount);
            for (int blocksRead = 0, currentLine = 1; blocksRead < blockCount; blocksRead++)
            {
                //pobierz wymiary klocka
                numbers = lines[currentLine].TrimEnd().Split(Separator);
                Debug.Assert(numbers.Length == 2, "Błędny opis wymiarów klocka");
                var blockWidth = int.Parse(numbers[0]);
                var blockHeight = int.Parse(numbers[1]);
                //wczytaliśmy następną linię
                currentLine++;
                //wczytaj komórki klocka
                var blockFields = new bool[blockHeight, blockWidth];
                //zliczamy pole klocka
                var blockArea = 0;
                for (var blockLine = 0; blockLine < blockHeight; blockLine++, currentLine++)
                {
                    numbers = lines[currentLine].Split(Separator);
                    for (var blockColumn = 0; blockColumn < blockWidth; blockColumn++)
                    {
                        var blockField = numbers[blockColumn];
                        Debug.Assert(blockField == "1" || blockField == "0", "Błędny opis komórki klocka", "Linia {0}: w komórce [{1}, {2}] znajduje się \"{3}\"", currentLine, blockLine, blockCount, blockField);
                        var field = blockField == "1";
                        blockFields[blockLine, blockColumn] = field;
                        if (field) blockArea++;
                    }
                }
                //mamy wszystkie dane do konstrukcji klocka
                var block = new Block(blockFields, blockArea, blocksRead);
                blocks.Add(block);
            }
            return new TaioTask(wellWidth, blocks);
        }
    }
}
