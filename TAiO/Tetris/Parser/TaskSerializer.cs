﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Parser
{
    public class TaskSerializer
    {
        /// <summary>
        /// Method to serialize task
        /// </summary>
        /// <param name="file">Path to the output file</param>
        /// <param name="taioTask">Task to serialize</param>
        public void Serialize(string file, TaioTask taioTask)
        {
            using (var fileStream=new FileStream(file,FileMode.Open,FileAccess.ReadWrite))
            {
                using (var writer =new StreamWriter(fileStream))
                {
                    Serialize(writer, taioTask);
                }
            }
        }
        /// <summary>
        /// Method to serialize task
        /// </summary>
        /// <param name="writer">Writer</param>
        /// <param name="taioTask">The task to serialize</param>
        public void Serialize(StreamWriter writer, TaioTask taioTask)
        {
            writer.WriteLine("{0} {1}",taioTask.WellWidth, taioTask.AvailableBlocks.Count);
            foreach (var block in taioTask.AvailableBlocks)
            {
                writer.WriteLine("{0} {1}", block.Width, block.Height);
                for (int i = 0; i < block.Height; i++)
                {
                    writer.Write(FieldValueString(block.OccupiedSpace[i, 0]));
                    for (int j = 1; j < block.Width; j++)
                    {
                        writer.Write(" {0}", FieldValueString(block.OccupiedSpace[i,j]));
                    }
                    writer.WriteLine();
                }
            }
            writer.Flush();
        }

        protected static string FieldValueString(bool fieldValue) => fieldValue ? "1" : "0";
    }
}
