﻿using System;
using System.Windows.Media;

namespace Tetris.Model
{
    [Serializable]
    public class ColorTetris
    {
        protected static readonly Random Rand = new Random();
        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }
        public byte A { get { return 255; } }
        private int rR = Rand.Next(50, 200);
        private int rG = Rand.Next(50, 200);
        private int rB = Rand.Next(50, 200);
        private int max = 256;

        public ColorTetris()
        {
            R = (byte)Rand.Next(max);
            G = (byte)Rand.Next(max);
            B = (byte)Rand.Next(max);
        }
        /// <summary>
        /// Method to get random color
        /// </summary>
        /// <returns>Color</returns>
        public Color GetColor()
        {
            return Color.FromArgb(A, R, G, B);
        }
        /// <summary>
        /// Method to set color for duplicated blocks
        /// </summary>
        /// <param name="q">The wage</param>
        public void SetColor(int q = 0)
        {
            R = (byte)((R + rR * q) % max);
            G = (byte)((G + rG * q) % max);
            B = (byte)((B + rB * q) % max);
        }
    }
}
