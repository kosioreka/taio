﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model
{
    /// <summary>Class representing importing of the task</summary>
    public class TaioTask
    {
        public TaioTask() : this(0, new List<Block>()) { }
        public TaioTask(int wellWidth, List<Block> availableBlocks)
        {
            WellWidth = wellWidth;
            AvailableBlocks = availableBlocks;
        }

        /// <summary>Width of the well</summary>
        public int WellWidth { get; }

        /// <summary>Types of available blocks</summary>
        public List<Block> AvailableBlocks { get; }
    }
}
