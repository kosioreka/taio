﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model
{
    [Serializable]
    public class Well
    {
        #region variables
        /// <summary>Well width</summary>
        public int Width
        {
            get; set;
        }

        /// <summary>Levels of well starting from the top</summary>
        public List<List<int>> Lines
        {
            get; set;
        }

        /// <summary>
        /// Actual height of blocks
        /// </summary>
        public int ActualHeight
        {
            get
            {
                return Lines.Count;
            }
        }

        /// <summary>
        /// List of blocks added to the well
        /// </summary>
        public List<Block> BlocksAdded;

        /// <summary>
        /// Rate of the well used in evaluation of well
        /// </summary>
        public double Rate
        {
            get; set;
        }

        /// <summary>
        /// Number of fields filled in the well
        /// </summary>
        public int FilledArea
        {
            get; set;
        }

        /// <summary>
        /// The last added block to the well
        /// </summary>
        public Block LastAddedBlock
        {
            get; set;
        }
        /// <summary>
        /// Quantities of blocks used in this well. Indexes correspond to indexes in Blocks list in AlgorithmStep class
        /// </summary>
        public int[] QuantitiesUsed
        {
            get; set;
        }
        /// <summary>
        /// The last minimal line that was used
        /// </summary>
        public int LastMinLine
        {
            get; set;
        }
        /// <summary>
        /// The maximum smaller size of used blocks
        /// </summary>
        public int MaxMinSize
        {
            get; set;
        }
        /// <summary>
        /// Number of empty fields in the well
        /// </summary>
        public int EmptyFields { get; set; }
        /// <summary>
        /// Percent of well filled
        /// </summary>
        public double PercentageFilled
        {
            get
            {
                var div = (ActualHeight * Width);
                return div > 0 ? FilledArea * 100 / div : 0;
            }
        }
        #endregion

        public Well() : this(0) { }

        public Well(int numberOfBlocks, int width=1)
        {
            Width = width;
            Rate = 0;
            Lines = new List<List<int>>();
            LastMinLine = 0;
            LastAddedBlock = new Block(new bool[0, 0], 0);
            QuantitiesUsed = new int[numberOfBlocks];
            BlocksAdded = new List<Block>();
        }
        /// <summary>
        /// The importance of fill in the Rate function
        /// </summary>
        protected int FillWage = 10;

        /// <summary>
        /// Whether there is a block that has one bigger size than the well width
        /// </summary>
        protected bool IsBigger { get; set; }
        /// <summary>
        /// Number of blocks that have one bigger size than the well width
        /// </summary>
        protected int NumberOfBigger { get; set; }
        /// <summary>
        /// The wage for well with block that has one bigger size than the well width
        /// </summary>
        protected int BiggerWage { get; set; } = 100;
        /// <summary>
        /// Method to compute Rate property
        /// </summary>
        /// <param name="lastAdjacentBlockNumber">Number of adjacent fields to the block</param>
        public void ComputeRate(int lastAdjacentBlockNumber)
        {
            Rate = (double) FilledArea/(ActualHeight*Width)*FillWage //fill rate
                   //we want the block to be adjacent to as many previously put blocks or well walls as possible
                   + (double) lastAdjacentBlockNumber/LastAddedBlock.Area
                   - (double) EmptyFields - Lines[LastMinLine].Count(a => a == -1)
            ;
            if (IsBigger)
                Rate += BiggerWage*NumberOfBigger;
        }

        #region block addition
        /// <summary>
        /// Method to add block to the well
        /// </summary>
        /// <param name="b">Block to add</param>
        /// <param name="index">Block index in quantities table</param>
        /// <returns>True if block can be placed in this rotation</returns>
        public bool AddBlock(Block b, int index)
        {
            LastAddedBlock = b;
            FilledArea += b.Area;
            QuantitiesUsed[index]++;
            MaxMinSize = Math.Max(MaxMinSize, Math.Min(b.Width, b.Height));
            int x, y;
            List<List<bool>> bLines = LastAddedBlock.GetLines();
            if (bLines[0].Count > Width)
                return false;
            FindPosition(out x, out y, bLines);
            PutBlock(x, y, bLines);
            LastAddedBlock.OffsetX = x;
            LastAddedBlock.OffsetY = y;
            b.Color.SetColor(QuantitiesUsed[index]-1);
            BlocksAdded.Add(b);
            return true;
        }
        /// <summary>
        /// Adds block to the well
        /// </summary>
        /// <param name="x">X block index</param>
        /// <param name="y">Y block index</param>
        /// <param name="bLines">Lines of block</param>
        protected void PutBlock(int x, int y, List<List<bool>> bLines)
        {
            var lastAdjacentNumber = 0;
            int bw = bLines[0].Count; //block width
            for (int i = 0; i < bLines.Count; i++)
            {
                if (i + y >= Lines.Count)
                {
                    Lines.Add(new List<int>(Enumerable.Repeat(-1, Width)));
                    //Lines[i+y].AddRange(new bool[Width]);
                }
                for (int j = 0; j < bw; j++)
                {
                    if (bLines[i][j])
                    {
                        CountAdjacent(j, i, j + x, i + y, bLines, ref lastAdjacentNumber);
                        Lines[i + y][j + x] = BlocksAdded.Count;
                    }
                }
            }
            if (bLines.Count > Width)
            {
                IsBigger = true;
                NumberOfBigger++;
            }
            ComputeRate(lastAdjacentNumber);
        }

        /// <summary>
        /// Method to add adjacent fields to the block being put
        /// </summary>
        /// <param name="i">X field index in the block</param>
        /// <param name="j">Y field index in the block</param>
        /// <param name="x">X field index in the well</param>
        /// <param name="y">Y field index in the well</param>
        /// <param name="bLines">Lines of the block</param>
        /// <param name="adjacentFields">Number of adjacent fields to the block</param>
        protected void CountAdjacent(int i, int j, int x, int y, List<List<bool>> bLines, ref int adjacentFields)
        {
            //if i is the block edge
            if (i - 1 < 0)
            {
                //if x is the well edge - block field is adjacent to the well wall
                if (x - 1 < 0)
                {
                    adjacentFields++;
                }
                else //it's still inside of the well
                {
                    //if there was a block in this field - new block is adjacent to it
                    if (Lines[y][x - 1] >= 0)
                    {
                        adjacentFields++;
                    }
                }
                
            }
            else if (i + 1 >= bLines[0].Count) //second block edge
            {
                //the same - just the second wall
                if (x + 1 >= Lines[0].Count)
                {
                    adjacentFields++;
                }
                else
                {
                    if (Lines[y][x + 1] >= 0)
                    {
                        adjacentFields++;
                    }
                }
            }
            else //i is inside of the block so it's also inside of the well
            {
                //if the taken field in the well isn't from the new block
                if (!bLines[j][i - 1] && Lines[y][x - 1] >= 0)
                {
                    adjacentFields++;
                }
                if (!bLines[j][i + 1] && Lines[y][x + 1] >= 0)
                {
                    adjacentFields++;
                }
            }

            //the same but for j
            if (j - 1 < 0)
            {
                //if y is the well edge - block field is adjacent to the well wall
                if (y - 1 < 0)
                {
                    adjacentFields++;
                }
                else //it's still inside of the well
                {
                    //if there was a block in this field - new block is adjacent to it
                    if (Lines[y-1][x] >= 0)
                    {
                        adjacentFields++;
                    }
                }
            }
            else if (j + 1 >= bLines.Count) //second block edge
            {
                //well has only 3 walls
                if (y + 1 < Lines.Count && Lines[y+1][x] >= 0)
                {
                    adjacentFields++;
                }
            }
            else //j is inside of the block so it's also inside of the well
            {
                //if the taken field in the well isn't from the new block
                if (!bLines[j-1][i] && Lines[y-1][x] >= 0)
                {
                    adjacentFields++;
                }
                if (!bLines[j+1][i] && Lines.Count> y+1 && Lines[y+1][x] >= 0)
                {
                    adjacentFields++;
                }
            }
        }

        /// <summary>
        /// Find indexes in the well for the corner of the block to add
        /// </summary>
        /// <param name="x">X block index</param>
        /// <param name="y">Y block index</param>
        /// <param name="bLines">Lines of block</param>
        protected void FindPosition(out int x, out int y, List<List<bool>> bLines)
        {
            //first used field in block in first line
            int fb = bLines[0].FindIndex(a => a);
            //lazy adding empty lines for new well
            if (LastMinLine == Lines.Count) AddEmptyWellLine();
            //first free field in well in it's min possible line
            int fw = Lines[LastMinLine].FindIndex(a => a == -1);
            while (!CanAddHere(fb, fw, bLines))
            {
                fw = Lines[LastMinLine].FindIndex(fw + 1, a => a == -1);
                while (fw < 0)
                {
                    EmptyFields += Lines[LastMinLine].Count(a => a == -1);
                    
                    LastMinLine++;
                    if (LastMinLine == Lines.Count)
                    {
                        AddEmptyWellLine();
                        fw = 0;
                    }
                    else
                    {
                        fw = Lines[LastMinLine].FindIndex(fw + 1, a => a == -1);
                    }
                }
            }
            x = fw-fb;
            y = LastMinLine;
        }
        /// <summary>
        /// Method to add empty line to the well
        /// </summary>
        protected void AddEmptyWellLine()
        {
            Lines.Add(new List<int>(Enumerable.Repeat(-1, Width)));
        }

        /// <summary>
        /// Method checking if block can be put at specified place
        /// </summary>
        /// <param name="fb">Index of block first taken field in the first line</param>
        /// <param name="fw">Index of Well current first free space in the line</param>
        /// <param name="bLines">Lines of block</param>
        /// <returns>True if block can be placed there, false otherwise</returns>
        protected bool CanAddHere(int fb, int fw, List<List<bool>> bLines)
        {
            int bw = bLines[0].Count; //block width
            //if there is too little space to the well wall
            if (bw - fb > Width - fw || fb > fw)
            {
                return false;
            }
            fw -= fb;
            for (int i = 0; i < bLines.Count; i++)
            {
                int y = i + LastMinLine;
                //there was no such line, so the whole is free
                if (y >= Lines.Count)
                {
                    return true;
                }
                for (int j = 0; j < bw; j++)
                {
                    //if the field is already taken
                    if (Lines[y][fw + j] >= 0 && bLines[i][j])
                    {
                        return false;
                    }
                }
            }
            //all fields where free, block can be placed there
            return true;
        }
        #endregion

        /// <summary>
        /// Method to return k best wells (without duplicates) from the given list
        /// </summary>
        /// <param name="wells">List of wells to choose from</param>
        /// <param name="k">Max number of wells to return</param>
        /// <returns>List of best wells</returns>
        public static List<Well> GetBestWells(List<Well> wells, int k)
        {

            wells.Sort((a, b) => a.Rate < b.Rate ? 1 : a.Rate > b.Rate ? -1 : 0);
            wells = wells.Distinct(new WellComparer()).ToList();
            //temp.Sort((a, b) => a.Rate > b.Rate ? 1 : -1);
            wells = wells.Count > k ? wells.GetRange(0, k) : wells;
            return wells;
        }
    }
    /// <summary>
    /// Helper class to compare wells
    /// </summary>
    public class WellComparer : IEqualityComparer<Well>
    {
        public bool Equals(Well x, Well y)
        {
            if (x.ActualHeight != y.ActualHeight || x.FilledArea != y.FilledArea
                || x.EmptyFields!=y.EmptyFields)
                return false;
            for (int i = 0; i < x.ActualHeight; i++)
            {
                if (LineSum(x,i) != LineSum(y,i))
                    return false;
            }
            return true;
        }
        /// <summary>
        /// Sum of the lines in the well
        /// </summary>
        /// <param name="w">Well</param>
        /// <param name="index">Index of field</param>
        /// <returns>Sum</returns>
        private int LineSum(Well w, int index)
        {
            int sum = 0;
            foreach(var i in w.Lines[index])
            {
                sum += i >= 0 ? w.BlocksAdded[i].Index : i;
            }
            return sum;
        }

        public int GetHashCode(Well obj)
        {
            return obj.FilledArea;
        }
    }
}
