﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Tetris.Model
{
    /// <summary>Class representing block in the puzzle</summary>
    [Serializable]
    public class Block
    {
        public Block() : this(new bool[0, 0], 0) { }

        public Block(bool[,] occupiedSpace, int area, int index = 0)
        {
            if (occupiedSpace == null) throw new ArgumentNullException(nameof(occupiedSpace));
            OccupiedSpace = occupiedSpace;
            Area = area;
            Color = new ColorTetris();
            Index = index;
        }

        #region variables
        /// <summary>
        /// Number of possible radiuses
        /// </summary>
        public const int NumberOfRadiuses = 4;
        /// <summary>
        /// Block index
        /// </summary>
        public int Index { get; set; }

        protected int _radius;
        /// <summary>
        /// Rotation radius (clockwise): 360/NumberOfRadiuses*i 
        /// (e.g. for NumberOfRadiuses=4: 0 - 0, 1 - 90, 2 - 180, 3 - 270)
        /// </summary>
        public int Radius
        {
            get
            {
                return _radius;
            }
            set
            {
                _radius = value % NumberOfRadiuses;
            }
        }

        /// <summary>
        /// X offset of block position in the well (from the right)
        /// </summary>
        public int OffsetX
        {
            get; set;
        }
        /// <summary>
        /// Y offset of block position in the well (from the bottom)
        /// </summary>
        public int OffsetY
        {
            get; set;
        }
        #endregion
        
        /// <summary>The block's shape</summary>
        [XmlIgnore]
        public bool[,] OccupiedSpace { get; private set; }

        [XmlElement(ElementName = "OccupiedSpace")]
        public string Occupied {
            get
            {
                if (OccupiedSpace == null || OccupiedSpace.Length == 0) return string.Empty;
                var builder = new StringBuilder((Width + 1)*Height);
                for (var i = 0; i < Height; i++)
                {
                    for (var j = 0; j < Width; j++)
                    {
                        builder.Append(OccupiedSpace[i, j] ? '1' : '0');
                    }
                    builder.Append(';');
                }
                return builder.ToString();
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    OccupiedSpace = new bool[0, 0];
                    return;
                }
                var rows = value.Split(new [] {';'}, StringSplitOptions.RemoveEmptyEntries);
                var occupiedSpace = new bool[rows.Length, rows[0].Length];
                for (var i = 0; i < rows.Length; i++)
                {
                    for (var j = 0; j < rows[i].Length; j++)
                    {
                        if (rows[i][j] == '1') occupiedSpace[i, j] = true;
                    }
                }
                OccupiedSpace = occupiedSpace;
            }
        }

        /// <summary>Number of used areas</summary>
        public int Area { get; }
        /// <summary>
        /// The block height
        /// </summary>
        [XmlAttribute]
        public int Height { get { return OccupiedSpace.GetLength(0); } }
        /// <summary>
        /// The block width
        /// </summary>
        [XmlAttribute]
        public int Width { get { return OccupiedSpace.GetLength(1); } }
        /// <summary>
        /// Color
        /// </summary>
        public ColorTetris Color { get; set; }

        #region lines
        /// <summary>
        /// Method to get list of lines, starting from the top one at the current Radius
        /// </summary>
        /// <returns>List of lines</returns>
        public List<List<bool>> GetLines()
        {
            switch (Radius)
            {
                case 0:
                    return GetOLines();
                case 1:
                    return Get9OLines();
                case 2:
                    return Get18OLines();
                case 3:
                    return Get27OLines();
            }
            return null;
        }
        
        /// <summary>
        /// Get list of lines when Radius is 0
        /// </summary>
        /// <returns>List of lines</returns>
        protected List<List<bool>> GetOLines()
        {
            List<List<bool>> lines = new List<List<bool>>();
            for (int i = 0; i < Height; i++)
            {
                List<bool> line = new List<bool>();
                for (int j = 0; j < Width; j++)
                {
                    line.Add(OccupiedSpace[i, j]);
                }
                lines.Add(line);
            }
            return lines;
        }
        /// <summary>
        /// Get list of lines when Radius is 90
        /// </summary>
        /// <returns>List of lines</returns>
        protected List<List<bool>> Get9OLines()
        {
            List<List<bool>> lines = new List<List<bool>>();
            for (int i = 0; i < Width; i++)
            {
                List<bool> line = new List<bool>();
                for (int j = Height - 1; j >= 0; j--)
                {
                    line.Add(OccupiedSpace[j, i]);
                }
                lines.Add(line);
            }
            return lines;
        }
        /// <summary>
        /// Get list of lines when Radius is 180
        /// </summary>
        /// <returns>List of lines</returns>
        protected List<List<bool>> Get18OLines()
        {
            List<List<bool>> lines = new List<List<bool>>();
            for (int i = Height-1; i >=0; i--)
            {
                List<bool> line = new List<bool>();
                for (int j = Width - 1; j >= 0; j--)
                {
                    line.Add(OccupiedSpace[i, j]);
                }
                lines.Add(line);
            }
            return lines;
        }

        /// <summary>
        /// Get list of lines when Radius is 270
        /// </summary>
        /// <returns>List of lines</returns>
        protected List<List<bool>> Get27OLines()
        {
            List<List<bool>> lines = new List<List<bool>>();
            for (int i = Width-1; i >=0; i--)
            {
                List<bool> line = new List<bool>();
                for (int j = 0; j <Height; j++)
                {
                    line.Add(OccupiedSpace[j, i]);
                }
                lines.Add(line);
            }
            return lines;
        }
        #endregion
        #region comparison
        public override bool Equals(object obj)
        {
            var other = obj as Block;
            if (other == null)
                return false;
            if (OccupiedSpace == other.OccupiedSpace) return true;
            if (other.Area != Area) return false;
            //różne wymiary prostokątów opisanych
            if ((other.Height != Height || other.Width != Width) &&
                (other.Height != Width || other.Width != Height)) return false;
            
            var h = Height;
            var w = Width;
            return 
                (h == other.Height && w == other.Width &&
                    (EqualsRotate0(h, w, OccupiedSpace, other.OccupiedSpace) || EqualsRotate180(h, w, OccupiedSpace, other.OccupiedSpace))) 
                || (h == other.Width && w == other.Height &&
                    (EqualsRotate90(h, w, OccupiedSpace, other.OccupiedSpace) || EqualsRotate270(h, w, OccupiedSpace, other.OccupiedSpace)));
        }

        protected static bool EqualsRotate0(int h, int w, bool[,] tab1, bool[,] tab2)
        {
            for (var i = 0; i < h; i++)
            {
                for (var j = 0; j < w; j++)
                {
                    if (tab1[i, j] != tab2[i, j]) return false;
                }
            }
            return true;
        }

        protected static bool EqualsRotate180(int h, int w, bool[,] tab1, bool[,] tab2)
        {
            for (var i = 0; i < h; i++)
            {
                for (var j = 0; j < w; j++)
                {
                    if (tab1[i, j] != tab2[h - i - 1, w - j - 1]) return false;
                }
            }
            return true;
        }

        protected static bool EqualsRotate90(int h, int w, bool[,] tab1, bool[,] tab2)
        {
            for (var i = 0; i < h; i++)
            {
                for (var j = 0; j < w; j++)
                {
                    if (tab1[i, j] != tab2[j, h - i - 1]) return false;
                }
            }
            return true;
        }

        protected static bool EqualsRotate270(int h, int w, bool[,] tab1, bool[,] tab2)
        {
            for (var i = 0; i < h; i++)
            {
                for (var j = 0; j < w; j++)
                {
                    if (tab1[i, j] != tab2[w - j -1, i]) return false;
                }
            }
            return true;
        }
        
        public override int GetHashCode()
        {
            //klocki o tej samej powierzchni i liczbie pól
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return Area | OccupiedSpace.Length >> 8;
        }
        #endregion
    }
}
