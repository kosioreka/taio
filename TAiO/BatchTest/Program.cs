﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using GUI.ViewModel;
using Tetris.Model;
using Tetris.Parser;

namespace BatchTest
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length == 0) { PrintUsage(); return; }

            var taskFilePath = args[0];
            var outputFilePath = args.Length >= 2
                ? args[1]
                : taskFilePath + '_' + DateTime.Now.ToString("yyyyMMdd_HH_mm_ss") + ".csv";

            var task = LoadTask(taskFilePath);
            if (task == null) return;

            PerformTests(task, outputFilePath);
        }

        private static void PerformTests(TaioTask task, string outputFilePath)
        {
            VmWarmup(task);
            const int maxRuns = 10;
            const string separator = ";";
            var fakeVM = new MainWindowVM(task);
            AdjustBlockQuatntities(fakeVM);
            var blocks = task.AvailableBlocks;
            var blockTemplates = fakeVM.BlockTemplates;

            var results = new List<string>();
            //header line
            results.Add(string.Join(separator, "width", "k", "time(ms)", "well number", "well height", "fill rate", "rate"));
            var decreasedWidth = task.WellWidth - 5;
            var width = task.AvailableBlocks.Any(b => b.Width > decreasedWidth && b.Height > decreasedWidth) ? task.WellWidth : decreasedWidth;
            for (var run = 0; run < maxRuns; run++, width++)
            {
                fakeVM.TaioTask = new TaioTask(width, blocks);
                fakeVM.BlockTemplates = blockTemplates;
                fakeVM.ResetTask();
                var watch = new Stopwatch();
                watch.Start();
                TestChangingK(fakeVM, width, results, separator);
                watch.Stop();
                Console.WriteLine(@"Run {0,2} width:{1,2} computed in {2,7}ms", run + 1, width, watch.ElapsedMilliseconds);
            }
            SaveResults(results, outputFilePath);
        }

        private static void TestChangingK(MainWindowVM fakeVM, int width, List<string> results, string separator)
        {
            const int maxK = 10;
            for (var k = 1; k < maxK; k++)
            {
                fakeVM.K = k;
                fakeVM.StepNumber = fakeVM.AmountOfBlocks;
                fakeVM.AlgorithmWrapper.NextStep();
                var msTaken = fakeVM.AlgorithmWrapper.Time;
                Console.WriteLine(@"Step {0} computed in {1,7}ms", k, msTaken);
                results.AddRange(
                    fakeVM.AlgorithmWrapper.WellBlocks.Select(
                        wellBlock =>
                            string.Join(separator, width, k, msTaken, wellBlock.Number, wellBlock.Well.ActualHeight,
                                wellBlock.Well.PercentageFilled.ToString("0.0##"), wellBlock.Well.Rate.ToString("0.000"))));
            }
        }

        private static void VmWarmup(TaioTask task)
        {
            //perform some calculations so that JIT may do some work
            var fakeVM = new MainWindowVM(task, 2);
            fakeVM.StepNumber = fakeVM.AmountOfBlocks;
            fakeVM.AlgorithmWrapper.NextStep();
            GC.Collect();
        }

        private static void SaveResults(List<string> results, string outputFilePath)
        {
            File.WriteAllLines(outputFilePath, results);
            Console.WriteLine(@"Saved into {0}", outputFilePath);
        }

        private static void AdjustBlockQuatntities(MainWindowVM fakeVM)
        {
            const int requiredBlocks = 100;
            if (fakeVM.AmountOfBlocks >= requiredBlocks) return;
            var multiplier = requiredBlocks % fakeVM.AmountOfBlocks == 0
                ? requiredBlocks / fakeVM.AmountOfBlocks
                : requiredBlocks / fakeVM.AmountOfBlocks + 1;
            foreach (var blockTemplate in fakeVM.BlockTemplates)
            {
                blockTemplate.Number = multiplier;
            }
            fakeVM.ResetTask();
        }

        private static TaioTask LoadTask(string taskFilePath)
        {
            return new TaskParser().ParseFile(taskFilePath);
        }

        private static void PrintUsage()
        {
            Console.WriteLine(@"Taio Batch Testing Utility
Arguments:
task_file_path [output_file_path]
output_file_path defaults to (task_file_name)_(test_start_date)");
        }
    }
}
