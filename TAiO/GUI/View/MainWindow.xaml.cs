using System;
using System.Collections.ObjectModel;
using System.Windows;
using GUI.Logic;
using GUI.ViewModel;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Tetris.Parser;

namespace GUI.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindowVM MainWindowVM
        {
            get { return DataContext as MainWindowVM; }
            set { DataContext = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
            MainWindowVM = new MainWindowVM();
        }

        protected void BrowseBlocksButton_OnClick(object sender, RoutedEventArgs e)
        {
            var blockBrowserVM = new BlockBrowserVM
            {
                Blocks = CopyTemplates(MainWindowVM.BlockTemplates) ?? new ObservableCollection<BlockWrapper>()
            };

            BlockBrowser bb = new BlockBrowser(blockBrowserVM);
            bb.Closed += Bb_Closed;
            bb.Show();
        }

        /// <summary>
        /// Copy block wrappers for safe block occurence number editing
        /// </summary>
        /// <param name="blockTemplates">Old block templates</param>
        /// <returns>Collection to be used with edit window</returns>
        private static ObservableCollection<BlockWrapper> CopyTemplates(ObservableCollection<BlockWrapper> blockTemplates)
        {
            if (blockTemplates == null || blockTemplates.Count == 0) return null;
            var templates=new ObservableCollection<BlockWrapper>();
            foreach (var oldTemplate in blockTemplates)
            {
                templates.Add(new BlockWrapper(oldTemplate.Block, oldTemplate.Number));
            }
            return templates;
        }

        private void Bb_Closed(object sender, EventArgs e)
        {
            var vm = (sender as BlockBrowser)?.VM;
            if (vm == null || !vm.Saved) return;
            MainWindowVM.BlockTemplates = CopyTemplates(vm.Blocks);
            MainWindowVM.ResetTask();
        }

        private void ImportFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if(fileDialog.ShowDialog() == true)
            {
                TaskParser taskParser = new TaskParser();
                try
                {
                    var result = taskParser.ParseFile(fileDialog.FileName);
                    foreach (var b in result.AvailableBlocks)
                    {
                        if (b.Width > result.WellWidth && b.Height > result.WellWidth)
                        {
                            ErrorNotifyDialog("Some blocks are wider than the well!");
                            return;
                        }
                    }
                    MainWindowVM.SetNewTaioTask(result);
                }
                catch
                {
                    ErrorNotifyDialog("Error parsing file! Check if the file is correct.");
                }
            }
        }

        private void ForwardCalculation_Click(object sender, RoutedEventArgs e)
        {
            int diff = MainWindowVM.NextStep();
            if (diff < 0)
            {
                ErrorNotifyDialog("You can do up to " + Math.Abs(diff) + " steps, because there is not enough blocks left!");
            }
            else if (diff == 0)
            {
                ErrorNotifyDialog("End of the algorithm!");
            }
        }
        private async void ErrorNotifyDialog(string errorMsg)
        {
            await this.ShowMessageAsync("Error occurred", errorMsg);
        }

        private const string TaioTaskStateExtension = ".tts";
        private const string StateFilesFilter = "Saved state files|*.tts|Text files|*.txt|All files|*.*";
        private void LoadState_OnClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                DefaultExt = TaioTaskStateExtension,
                Filter = StateFilesFilter,
                AddExtension = true,
                Title = "Load calculation state"
            };
            if (!openFileDialog.ShowDialog().GetValueOrDefault()) return;
            try
            {
                var loadedVM = StateSerializationLogic.LoadState(openFileDialog.FileName);
                MainWindowVM = loadedVM;
            }
            catch (SystemException)
            {
                ErrorNotifyDialog("Exception has been thrown while loading saved calculation");
            }
        }

        private void SaveState_OnClick(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                DefaultExt = TaioTaskStateExtension,
                Filter = StateFilesFilter,
                AddExtension = true,
                Title = "Save calculation state",
                OverwritePrompt = true
            };
            if (!saveFileDialog.ShowDialog().GetValueOrDefault()) return;
            try
            {
                StateSerializationLogic.SaveState(saveFileDialog.FileName, MainWindowVM);
            }
            catch (SystemException)
            {
                ErrorNotifyDialog("Exception has been thrown saving calculation state");
            }
        }
        private void ResetCalculation_Click(object sender, RoutedEventArgs e)
        {
            MainWindowVM.ResetTask();
        }
    }
}
