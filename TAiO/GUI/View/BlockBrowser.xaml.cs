﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GUI.ViewModel;

namespace GUI.View
{
    /// <summary>
    /// Interaction logic for BlockBrowser.xaml
    /// </summary>
    public partial class BlockBrowser
    {
        /// <summary>
        /// The ViewModel for block browser
        /// </summary>
        public BlockBrowserVM VM
        {
            get
            {
                return DataContext as BlockBrowserVM;
            }
            set
            {
                DataContext = value;
            }
        }

        public BlockBrowser() : this(new BlockBrowserVM())
        {
        }

        public BlockBrowser(BlockBrowserVM vm)
        {
            InitializeComponent();
            DataContext = vm;
        }
        /// <summary>
        /// Method to save block quantities
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (VM != null)
                VM.Saved = true;
            this.Close();
        }
        /// <summary>
        /// Method to apply quantity to all of the blocks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApplyToAllButton_OnClick(object sender, RoutedEventArgs e)
        {
            var vm = (BlockBrowserVM)DataContext;
            int num = 0;
            if (!int.TryParse(AllNumberTextBox.Text, out num) || num < 0)
                return;
            for (int i = 0; i < vm.Blocks.Count; i++)
            {
                vm.Blocks[i].Number = num;
            }
        }
    }
}
