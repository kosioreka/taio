﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using GUI.Annotations;
using Tetris.Model;

namespace GUI.ViewModel
{
    public class MainWindowVM : INotifyPropertyChanged
    {
        public MainWindowVM()
        {
            _k = 1;
            BlockTemplates = new ObservableCollection<BlockWrapper>();
            AlgorithmWrapper = new AlgorithmWrapper(new List<Block>(), Quantities, WellWidth, K);
        }

        public MainWindowVM(TaioTask taioTask, int k = 1)
        {
            _taioTask = taioTask;
            _k = k;
            LoadTaskData();
            AlgorithmWrapper = new AlgorithmWrapper(BlocksTetris, Quantities, WellWidth, K);
            AlgorithmWrapper.SetWells();
        }

        #region Properties

        protected AlgorithmWrapper _algorithmWrapper;
        /// <summary>
        /// Takes care of the algorithm
        /// </summary>
        public AlgorithmWrapper AlgorithmWrapper
        {
            get { return _algorithmWrapper; }
            set
            {
                if(_algorithmWrapper == value) return;
                _algorithmWrapper?.StopExecution();//dispose old wrapper and it's background task
                _algorithmWrapper = value;
                OnPropertyChanged();
            }
        }

        /// <summary>Types of blocks that should be placed and their quantities</summary>
        public ObservableCollection<BlockWrapper> BlockTemplates { get; set; }

        /// <summary>
        /// Blocks from taio task
        /// </summary>
        public List<Block> BlocksTetris { get; set; }
        protected int _wellWidth;
        /// <summary>
        /// The well's width set from taio task file
        /// </summary>
        public int WellWidth
        {
            get { return _wellWidth; }
            set
            {
                _wellWidth = value;
                OnPropertyChanged();
            }
        }
        private int _k;
        /// <summary>
        /// The amount of the best well's to be shown
        /// </summary>
        public int K
        {
            get { return _k; }
            set
            {
                if (_k == value)
                    return;
                AlgorithmWrapper.Algorithm.K = _k = value;
                ResetTask();
                OnPropertyChanged();
            }
        }

        protected int _stepNumber;
        /// <summary>
        /// Number of steps to take in step mode
        /// </summary>
        public int StepNumber
        {
            get { return _stepNumber; }
            set
            {
                _stepNumber = value;
                OnPropertyChanged();
                AlgorithmWrapper.StepsToMake = value;
            }
        }

        protected TaioTask _taioTask;
        /// <summary>
        /// Current tast
        /// </summary>
        public TaioTask TaioTask
        {
            get { return _taioTask; }
            set
            {
                _taioTask = value;
                K = 1;
                StepNumber = 1;

                LoadTaskData();
                
                AlgorithmWrapper.SetRenew(BlocksTetris, Quantities, WellWidth, K);
                AlgorithmWrapper.SetWells();
            }
        }
        /// <summary>
        /// Method to load task data
        /// </summary>
        protected void LoadTaskData()
        {
            WellWidth = TaioTask.WellWidth;
            if (AlgorithmWrapper!=null) AmountOfBlocks = TaioTask.AvailableBlocks.Count;
            BlocksTetris = TaioTask.AvailableBlocks;
            LoadBlockTemplates(TaioTask.AvailableBlocks);
            CalculateBlocksAmount();
        }

        /// <summary>
        /// Executes when new taio task is loaded from the file
        /// </summary>
        /// <param name="task"></param>
        public void SetNewTaioTask(TaioTask task)
        {
            TaioTask = task;
        }

        /// <summary>
        /// Amount of particular blocks
        /// </summary>
        public int[] Quantities { get; set; }
        /// <summary>
        /// The number of blocks
        /// </summary>
        public int AmountOfBlocks
        {
            get { return AlgorithmWrapper.AmountOfBlocks; }
            set { AlgorithmWrapper.AmountOfBlocks = value; }
        }
        #endregion
        /// <summary>
        /// Calculates the amount of blocks
        /// </summary>
        public void CalculateBlocksAmount()
        {
            if(AlgorithmWrapper!=null) AmountOfBlocks = BlockTemplates.Sum(t => t.Number);
            Quantities = BlockTemplates.Select(t => t.Number).ToArray();
        }
        /// <summary>
        /// Method to reset the current task
        /// </summary>
        public void ResetTask()
        {
            CalculateBlocksAmount();
            AlgorithmWrapper.SetRenew(BlocksTetris, Quantities, WellWidth, K);
            AlgorithmWrapper.SetWells();
        }
        /// <summary>
        /// Executes next step for Algorithm
        /// </summary>
        internal int NextStep()
        {
            if (AmountOfBlocks >= AlgorithmWrapper.NumberOfBlocksAdded + StepNumber)
            {
                Task.Run((Action) AlgorithmWrapper.NextStep);
                return 1;
            }
            return AlgorithmWrapper.NumberOfBlocksAdded - AmountOfBlocks;
        }
        /// <summary>
        /// Method to load block templates
        /// </summary>
        /// <param name="blocks">Block templates</param>
        internal void LoadBlockTemplates(IEnumerable<Block> blocks)
        {
            var templates=new ObservableCollection<BlockWrapper>();
            foreach (var block in blocks)
            {
                templates.Add(new BlockWrapper(block));
            }
            BlockTemplates = templates;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
