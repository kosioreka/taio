﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GUI.Annotations;
using Tetris.Model;

namespace GUI.ViewModel
{
    public class BlockBrowserVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Collection of blocks to view
        /// </summary>
        public ObservableCollection<BlockWrapper> Blocks
        {
            get; set;
        }

        protected int _allNumber=1;
        /// <summary>
        /// Number setting for all of the blocks;
        /// </summary>
        public int AllNumber
        {
            get
            {
                return _allNumber;
            }
            set
            {
                if (value <= 0)
                    return;
                _allNumber = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// The original blocks
        /// </summary>
        public List<Block> BlockTemplates
        {
            get; set;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public BlockBrowserVM(List<Block> blocks)
        {
            InitializeBlocks(blocks);
        }

        public BlockBrowserVM()
        {
            InitializeBlocks(BlockTemplates);
        }
        /// <summary>
        /// Method to initialize blocks
        /// </summary>
        /// <param name="blocks">Blocks to initialize</param>
        protected void InitializeBlocks(List<Block> blocks)
        {
            var wrappers = new ObservableCollection<BlockWrapper>();
            //jeśli kolekcja będzie zmieniana gdy jest podpięta pod widok
            // po każdym dodaniu będzie przeliczany widok
            if (blocks != null)
            {
                foreach (var b in blocks)
                {
                    wrappers.Add(new BlockWrapper(b));
                }
            }
            Blocks = wrappers;
        }

        
        /// <summary>Indicates whether changes were saved</summary>
        public bool Saved
        {
            get; set;
        }

    }
}
