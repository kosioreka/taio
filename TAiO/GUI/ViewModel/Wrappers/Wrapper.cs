﻿using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using GUI.Annotations;
using Color = System.Windows.Media.Color;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace GUI.ViewModel
{
    public class Wrapper : INotifyPropertyChanged
    {
        #region properties
        protected int _number;
        protected BitmapSource _imageSource;
        protected int _size;
        /// <summary>
        /// Length and height of the Bitmap with block image
        /// </summary>
        public int Size
        {
            get
            {
                return _size;
            }
            set
            {
                if (value == _size)
                    return;
                _size = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Number of blocks
        /// </summary>
        public int Number
        {
            get
            {
                return _number;
            }
            set
            {
                if (value == _number)
                    return;
                _number = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Canvas representation of block
        /// </summary>
        public Canvas CanvasImage
        {
            get; set;
        }
        /// <summary>
        /// Bitmap image
        /// </summary>
        public Bitmap Image
        {
            get; set;
        }
        /// <summary>
        /// Path to show image
        /// </summary>
        public BitmapSource ImageSource
        {
            get
            {
                return _imageSource;
            }
            set
            {
                _imageSource = value;
                OnPropertyChanged();
            }
        }
        #endregion

        public Wrapper(int number = 1, int size = 100)
        {
            Number = number;
            Size = size <= 0 ? 100 : size;
        }

        /// <summary>
        /// Method that changes bool[,] into Canvas
        /// </summary>
        protected virtual void MakeImage()
        {
        }
        /// <summary>
        /// Method to paint blocks in the well
        /// </summary>
        /// <param name="unitSize">The size of unit square</param>
        /// <param name="color">Block color</param>
        /// <param name="i">Offset</param>
        /// <param name="j">Offest</param>
        /// <param name="lastBlock">Whether the block was added in the last step</param>
        /// <returns>Rectangle</returns>
        protected Rectangle GetUnitBox(int unitSize, Color color, int i, int j, bool lastBlock = false)
        {
            Rectangle rect = new Rectangle();
            rect.Width = unitSize;
            rect.Height = unitSize;
            rect.Fill = new SolidColorBrush(color);
            rect.Stroke = new SolidColorBrush(lastBlock ? Colors.Red : Colors.Black);
            rect.StrokeThickness = 1;
            Canvas.SetTop(rect, unitSize * i);
            Canvas.SetLeft(rect, unitSize * j);
            return rect;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
