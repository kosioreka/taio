﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using Tetris.Model;

namespace GUI.ViewModel
{
    public class BlockWrapper : Wrapper
    {
        protected Block _block;
        /// <summary>
        /// A block element
        /// </summary>
        public Block Block
        {
            get
            {
                return _block;
            }
            set
            {
                if (Equals(value, _block))
                    return;
                _block = value;
                OnPropertyChanged();
            }
        }

        public BlockWrapper(Block b, int number = 1, int size = 100): base(number, size)
        {
            Block = b;
            MakeImage();
        }
        /// <summary>
        /// Method to create bitmap image from Canvas
        /// </summary>
        protected override void MakeImage()
        {
            Color col = Block.Color.GetColor();
            Canvas temp = new Canvas();
            temp.Height = Size;
            temp.Width = Size;
            temp.Background = Brushes.White;
            //determining the size of one squere of block
            int unit = Math.Max(Block.Height, Block.Width);
            int width = Size / unit;
            for (int i = 0; i < Block.Height; i++)
            {
                for (int j = 0; j < Block.Width; j++)
                {
                    //Drawing square if in the block
                    if (Block.OccupiedSpace[i, j])
                    {
                        temp.Children.Add(GetUnitBox(width, col, i, j));
                    }
                }
            }         
            CanvasImage = temp;
            Image = CanvasConverters.SaveCanvasAsBitmap(temp);
            ImageSource = CanvasConverters.CreateBitmapSource(Image);
        }    
    }
}
