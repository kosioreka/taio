﻿using System.Collections.Generic;
using System.Windows.Controls;
using Tetris.Model;
using Brushes = System.Windows.Media.Brushes;

namespace GUI.ViewModel
{
    public class WellWrapper : Wrapper
    {
        protected Well _well;
        /// <summary>
        /// The current well
        /// </summary>
        public Well Well
        {
            get { return _well; }
            set
            {
                _well = value;
                OnPropertyChanged();
            }
        }
        public WellWrapper(Well w, int number = 1, int size = 20) : base(number, size)
        {
            Well = w;
            MakeImage();
        }
        /// <summary>
        /// Makes bitmap image from the well
        /// </summary>
        protected override void MakeImage()
        {
            Canvas temp = new Canvas();
            temp.Background = Brushes.White;
            int wellHeight = (Well.ActualHeight > Size ? Well.ActualHeight + 1 : Size);
            temp.Height = wellHeight * Size;
            temp.Width = Well.Width * Size;

            for (int i = 0; i < Well.ActualHeight; i++)
            {
                for (int j = 0; j < Well.Width; j++)
                {
                    int blockIndex = Well.Lines[i][j];
                    //Drawing square if in the block
                    if (blockIndex >= 0)
                    {
                        temp.Children.Add(
                            GetUnitBox(
                                Size, 
                                Well.BlocksAdded[blockIndex].Color.GetColor(), 
                                wellHeight - i - 1, 
                                Well.Width - j - 1, 
                                Well.BlocksAdded.Count-1 == blockIndex
                                )
                            );
                    }
                }

            }
            CanvasImage = temp;
            Image = CanvasConverters.SaveCanvasAsBitmap(temp);
            ImageSource = CanvasConverters.CreateBitmapSource(Image);
        }

    }
}
