﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using GUI.Annotations;
using Tetris.Algorithm;
using Tetris.Model;
using System.Diagnostics;

namespace GUI.ViewModel
{
    /// <summary>
    /// Manages the algorithm for UI
    /// </summary>
    public class AlgorithmWrapper : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Instance of algorithm
        /// </summary>
        public Algorithm Algorithm
        {
            get; set;
        }
        /// <summary>
        /// Thread used for continuous algorithm
        /// </summary>
        private Task _algTask;
        /// <summary>
        /// Should execution be aborted (execution cannot be restarted)
        /// </summary>
        private bool _stopExecution;
        /// <summary>
        /// Default width of the well. For displaying purpose
        /// </summary>
        private const int DefaultWellWidth = 10;
        /// <summary>
        /// The time between two steps in continuous algorithm
        /// </summary>
        private const int SleepingTime = 1000;
        /// <summary>
        /// Wells to be displayed
        /// </summary>
        private ObservableCollection<WellWrapper> _wellBlocks;
        /// <summary>
        /// Number of steps already made
        /// </summary>
        public int StepsDone
        {
            get; set;
        }
        /// <summary>
        /// Amount of steps to make
        /// </summary>
        public int StepsToMake
        {
            get; set;
        } = 1;
        /// <summary>
        /// Number of available types of blocks
        /// </summary>
        public int NumberOfBlocks
        {
            get; set;
        }
        /// <summary>
        /// Number of blocks in the wells
        /// </summary>
        public int NumberOfBlocksAdded
        {
            get
            {
                return Algorithm.BestWells.FirstOrDefault()?.BlocksAdded?.Count ?? 0;
            }
        }
        private int _amountOfBlocks;
        /// <summary>
        /// The amount of all blocks (with regard to quantities)
        /// </summary>
        public int AmountOfBlocks
        {
            get
            {
                return _amountOfBlocks;
            }
            set
            {
                _amountOfBlocks = value;
                OnPropertyChanged();

            }
        }
        protected bool _continuousAlg;
        /// <summary>
        /// The type of the algorithm
        /// </summary>
        public bool ContinuousAlg
        {
            get
            {
                return _continuousAlg;
            }
            set
            {
                _continuousAlg = value;
                OnPropertyChanged();
                if (value)
                {
                    _algTask = TimeoutNextStep();
                }
            }
        }
        protected long _time;
        /// <summary>
        /// Gets the time
        /// </summary>
        public long Time
        {
            get
            {
                return _time;
            }
            set
            {
                _time = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// The wells to show
        /// </summary>
        public ObservableCollection<WellWrapper> WellBlocks
        {
            get
            {
                return _wellBlocks;
            }
            set
            {
                _wellBlocks = value;
                OnPropertyChanged();
            }
        }
        #endregion
        public AlgorithmWrapper() : this(new List<Block>(), new int[0], 0)
        {
        }

        public AlgorithmWrapper(List<Block> blocksTetris, int[] quantities, int wellWidth, int k = 1)
        {
            NumberOfBlocks = blocksTetris.Count;
            WellBlocks = new ObservableCollection<WellWrapper>();
            Algorithm = new Algorithm(blocksTetris, quantities, wellWidth) { K = k };

        }
        /// <summary>
        /// Next step method used by the thread in continuous algorithm
        /// </summary>
        private async Task TimeoutNextStep()
        {
            while (!_stopExecution && ContinuousAlg)
            {
                if (AmountOfBlocks > StepsDone)
                {
                    NextStep();
                    await Task.Delay(SleepingTime);
                }
                else
                {
                    ContinuousAlg = false;
                }
            }
        }

        /// <summary>
        /// Stop task execution (cannot be restarted)
        /// </summary>
        public void StopExecution()
        {
            _stopExecution = true;
        }

        /// <summary>
        /// Carries out the next step of the algorithm
        /// </summary>
        public void NextStep()
        {
            if (NumberOfBlocks > 0)
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                Algorithm.Step(StepsToMake);
                stopwatch.Stop();
                Time = stopwatch.ElapsedMilliseconds;
                StepsDone += StepsToMake;
            }
            if (Application.Current != null)
            {
                //using with WPF interface
                Application.Current.Dispatcher.Invoke(() =>
                {
                    SetWells();
                    OnPropertyChanged(nameof(NumberOfBlocksAdded));
                });
            }
            else
            {
                SetWells();
                OnPropertyChanged(nameof(NumberOfBlocksAdded));
            }
        }
        /// <summary>
        /// Sets for best well's bitmap
        /// </summary>
        public void SetWells()
        {
            WellBlocks.Clear();
            if (Algorithm.BestWells?.Count > 0)
            {
                foreach (var w in Algorithm?.BestWells)
                {
                    WellBlocks.Add(new WellWrapper(w));
                }
            }
            else
            {
                for (int i = 0; i < Algorithm.K; ++i)
                {
                    WellBlocks.Add(new WellWrapper(new Well(NumberOfBlocks, DefaultWellWidth)));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        /// <summary>
        /// Starts new algorithm
        /// </summary>
        /// <param name="blocksTetris">Blocks to add</param>
        /// <param name="quantities">Blocks' quantities</param>
        /// <param name="wellWidth">The width of the well</param>
        /// <param name="k">The k parameter</param>
        internal void SetRenew(List<Block> blocksTetris, int[] quantities, int wellWidth, int k)
        {
            if (blocksTetris == null)
                return;
            NumberOfBlocks = blocksTetris.Count;
            WellBlocks.Clear();
            Algorithm = new Algorithm(blocksTetris, quantities, wellWidth) { K = k };
            StepsDone = 0;
            ContinuousAlg = false;
            OnPropertyChanged(nameof(NumberOfBlocksAdded));
        }
    }
}
