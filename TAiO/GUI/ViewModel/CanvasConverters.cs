﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GUI.ViewModel
{
    public class CanvasConverters
    {
        /// <summary>
        /// Method to convert canvas to bitmap
        /// </summary>
        /// <param name="surface">Canvas to convert</param>
        /// <returns>Converted bitmap</returns>
        public static Bitmap SaveCanvasAsBitmap(Canvas surface)
        {
            if (surface == null)
                return null;

            // Save current canvas transform
            Transform transform = surface.LayoutTransform;
            // reset current transform (in case it is scaled or rotated)
            surface.LayoutTransform = null;

            // Get the size of canvas
            System.Windows.Size size = new System.Windows.Size((int)surface.Width, (int)surface.Height);
            // Measure and arrange the surface
            // VERY IMPORTANT
            surface.Measure(size);
            surface.Arrange(new Rect(size));

            // Create a render bitmap and push the surface to it
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
              (int)size.Width,
              (int)size.Height,
              96d,
              96d,
              PixelFormats.Pbgra32);
            renderBitmap.Render(surface);


            //Restore previously saved layout
            surface.LayoutTransform = transform;

            //save RenderTargetBitmap as bitmap
            MemoryStream stream = new MemoryStream();
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
            encoder.Save(stream);

            return new Bitmap(stream);

        }

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        protected static extern bool DeleteObject(IntPtr hObject);
        /// <summary>
        /// Method to create BitmapSource from Bitmapt to view image in wpf
        /// </summary>
        /// <param name="bmp">Bitmap the source for which is to be created</param>
        /// <returns>Bitmap source</returns>
        public static BitmapSource CreateBitmapSource(Bitmap bmp)
        {
            IntPtr hbitmap = bmp.GetHbitmap();
            BitmapSource source = null;
            try
            {
                source = Imaging.CreateBitmapSourceFromHBitmap(
                   hbitmap, IntPtr.Zero, Int32Rect.Empty,
                   System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            }
            finally
            {
                // Clean up the bitmap data
                DeleteObject(hbitmap);
            }
            return source;
        }
        
    }
}
