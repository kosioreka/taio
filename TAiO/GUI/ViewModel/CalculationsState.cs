﻿using System;
using System.Collections.Generic;
using Tetris.Model;

namespace GUI.ViewModel
{
    /// <summary>
    /// Class used in serialization/deserialization of calculations progress
    /// </summary>
    [Serializable]
    public class CalculationsState
    {
        /// <summary>
        /// Mirrors MainWindowVM.K / Algorithm.K
        /// </summary>
        public int K { get; set; }
        /// <summary>
        /// Number of steps to do
        /// </summary>
        public int StepsToMake { get; set; }

        /// <summary>
        /// Mirrors Algorithm.WellWidth / MainWindowVM.WellWidth
        /// </summary>
        public int WellWidth { get; set; }
        
        /// <summary>
        /// Mirrors Algorithm.BestWells
        /// </summary>
        public List<Well> Wells { get; set; }
        
        /// <summary>
        /// Mirrors Algorithm.Quantities
        /// </summary>
        public int[] Quantities { get; set; }

        /// <summary>
        /// Mirrors Algorithm.Blocks
        /// Blocks.Count gives AlgorithmWrapper.NumberOfBlocks
        /// </summary>
        public List<Block> Blocks { get; set; }
    }
}