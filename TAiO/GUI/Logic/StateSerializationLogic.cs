﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using GUI.Annotations;
using GUI.ViewModel;
using Tetris.Model;
using Tetris.Parser;

namespace GUI.Logic
{
    public class StateSerializationLogic
    {
        /*
            po zastanowieniu postanowiłem napisać tak:
            Format pliku ze stanem obliczeń:
                na początku pliku jest zapis zadania(w formacie jak pliki wejściowe
                linia separująca
                serializowany obiekt opisujący stan obliczeń
        
            Sposób serializacji stanu obliczeń - XML
            wybór motywowany
                czytelność pliku
                łatwiejsza możliwość odczytu z napisu
            alternatywą była serializacja binarna ale nie można byłoby czytać pliku ani wczytywać części pliku bez tworzenia MemoryStream albo innych kombinacji

            */

        protected const string TaskEndMarker = "#####Koniec Zapisu Zadania#####";

        public static MainWindowVM LoadState(string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            using (var streamReader = new StreamReader(fileStream))
                return LoadState(streamReader);
        }

        public static MainWindowVM LoadState(StreamReader reader)
        {
            var task = DeserializeTask(reader);
            var calculationsSerialized = reader.ReadToEnd();
            var calculationsState = DeserializeCalculations(calculationsSerialized);
            return CreateVM(task, calculationsState);
        }

        /// <summary>Creates VM containing calculation state</summary>
        /// <param name="task"></param>
        /// <param name="state"></param>
        /// <returns>MainWindowVM ready to be attached to restore calculations state</returns>
        protected static MainWindowVM CreateVM(TaioTask task, CalculationsState state)
        {
            var algorithWrapper = new AlgorithmWrapper(state.Blocks, state.Quantities, state.WellWidth, state.K);
            algorithWrapper.Algorithm.BestWells = state.Wells;
            algorithWrapper.StepsToMake = 1;
            algorithWrapper.SetWells();
            var mainWindowVM = new MainWindowVM(task, state.K);
            mainWindowVM.AlgorithmWrapper = algorithWrapper;
            mainWindowVM.StepNumber = 1;
            mainWindowVM.CalculateBlocksAmount();
            return mainWindowVM;
        }

        protected static CalculationsState DeserializeCalculations(string calculationsSerialized)
        {
            using (var stringReader = new StringReader(calculationsSerialized))
                return (CalculationsState) new XmlSerializer(typeof (CalculationsState)).Deserialize(stringReader);
        }

        /// <summary>Reads task information placed in the beginning of the file</summary>
        /// <param name="reader">text reader providing task definition</param>
        /// <returns>Deserialized task used to start calculations</returns>
        protected static TaioTask DeserializeTask(TextReader reader)
        {
            var lines = new List<string>();
            while (true)
            {
                var lastLine = reader.ReadLine();
                if (null == lastLine) throw new FileFormatException();
                if (TaskEndMarker == lastLine) break;
                lines.Add(lastLine);
            }
            return new TaskParser().ParseLines(lines.ToArray());
        }
        
        public static void SaveState(string fileName, MainWindowVM mainWindowVM)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            using (var writer = new StreamWriter(fileStream))
                SaveState(writer, mainWindowVM);
        }

        public static void SaveState(StreamWriter writer, MainWindowVM mainWindowVM)
        {
            var calculationsData = GetCalculationsData(mainWindowVM);
            var task = mainWindowVM.TaioTask;
            new TaskSerializer().Serialize(writer,task);
            writer.WriteLine(TaskEndMarker);
            new XmlSerializer(typeof(CalculationsState)).Serialize(writer, calculationsData);
        }

        protected static CalculationsState GetCalculationsData(MainWindowVM mainWindowVM)
        {
            return new CalculationsState
            {
                K = mainWindowVM.K,

                WellWidth = mainWindowVM.WellWidth,

                Blocks = mainWindowVM.BlocksTetris,
                Quantities = mainWindowVM.Quantities,

                Wells = mainWindowVM.AlgorithmWrapper.Algorithm.BestWells,

                StepsToMake = 0 //TODO probably not needed
            };
        }
    }
}
