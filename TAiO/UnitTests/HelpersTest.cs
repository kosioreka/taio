﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris.Algorithm;
using Tetris.Model;

namespace UnitTests
{
    [TestClass]
    public class HelpersTest
    {
        [TestMethod]
        public void DeepCopyIntTest()
        {
            int a = 5;
            Assert.AreEqual(a, Helper.DeepCopy(a));
        }
        [TestMethod]
        public void DeepCopyBlockTest()
        {
            var b = new Block(new bool[,] { { true, true, false, false }, { true, true, true, false } }, 10, 10);
            Assert.AreEqual(b, Helper.DeepCopy(b));
        }
        [TestMethod]
        public void ColorModelTest()
        {
            var color = new ColorTetris();
            int max = 255;
            Assert.IsTrue(color.A <= max && color.B <= max && color.R <= max && color.G <= max);
            Assert.IsNotNull(color.GetColor());
        }
        [TestMethod]
        public void ColorSameTest()
        {
            var color = new ColorTetris();
            Assert.IsNotNull(color.GetColor());
            var color2 = Helper.DeepCopy(color);
            Assert.AreEqual(color.GetColor(), color2.GetColor());
            color2.SetColor();
            Assert.AreEqual(color.GetColor(), color2.GetColor());
            color2.SetColor(1);
            Assert.AreNotEqual(color.GetColor(), color2.GetColor());
        }
    }
}

