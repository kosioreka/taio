﻿using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris.Model;
using Tetris.Parser;

namespace UnitTests
{
    [TestClass]
    public class TaskSerializerTest
    {
        [TestMethod]
        public void TrivialTaskSerializerTest()
        {
            const string trivialOutput = @"1 1
1 1
1";
            var taioTask = new TaioTask(1, new List<Block> { new Block(new[,] { { true } }, 1) });
            RunTest(taioTask, trivialOutput);
        }

        [TestMethod]
        public void TetrisTaskSerializerTest()
        {
            const string tetrisOutput = @"4 7
4 1
1 1 1 1
3 2
1 1 1
0 0 1
3 2
1 1 1
1 0 0
2 2
1 1
1 1
3 2
0 1 1
1 1 0
3 2
1 1 1
0 1 0
3 2
1 1 0
0 1 1";
            var taioTask = new TaioTask(4, new List<Block>
            {
                new Block(new[,] {{true, true, true, true}}, 4), //I  "długi"
                new Block(new[,] {{true, true, true}, {false, false, true}}, 4), //J
                new Block(new[,] {{true, true, true}, {true, false, false}}, 4), //L
                new Block(new[,] {{true, true}, {true, true}}, 4), //O
                new Block(new[,] {{false, true, true}, {true, true, false}}, 4), //S
                new Block(new[,] {{true, true, true}, {false, true, false}}, 4), //T
                new Block(new[,] {{true, true, false}, {false, true, true}}, 4), //Z
            });
            RunTest(taioTask, tetrisOutput);
        }

        private static void RunTest(TaioTask taioTask, string expectedOutput)
        {
            var serializer = new TaskSerializer();
            string result;
            using (var memoryStream = new MemoryStream())
            {
                var writer = new StreamWriter(memoryStream);
                serializer.Serialize(writer, taioTask);
                //właściwie należałoby wywołać .Dispose dla writer, ale zamknie to memoryStream

                memoryStream.Position = 0;
                using (var reader = new StreamReader(memoryStream))
                {
                    result = reader.ReadToEnd();
                }
            }

            Assert.AreEqual(expectedOutput, result.Trim());
        }
    }
}
