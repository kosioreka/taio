﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris.Algorithm;
using Tetris.Model;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class AlgorithmTest
    {

        [TestMethod]
        public void StepMethodBestWellsCountTest1()
        {
            var alg = GetExampleAlgorithm();
            alg.Step();
            Assert.IsTrue(alg.BestWells.Count >= 0);
            Assert.IsTrue(alg.BestWells.Count == alg.K);
        }
        [TestMethod]
        public void StepMethodBestWellsCountTest2()
        {
            int k = 4;
            var alg = GetExampleAlgorithm(k);
            Assert.IsTrue(alg.K == k);
            alg.Step();
            Assert.IsTrue(alg.BestWells.Count >= 0);
            Assert.IsTrue(alg.BestWells.Count == alg.K);
        }

        [TestMethod]
        public void GetBestWellsTest()
        {
            int k = 5;
            var alg = GetExampleAlgorithm(k);
            Assert.IsTrue(alg.K == k);
            alg.Step();
            
            Assert.AreEqual(Well.GetBestWells(alg.BestWells, 4)[0], Well.GetBestWells(alg.BestWells, 1)[0]);
            Assert.IsTrue(Well.GetBestWells(alg.BestWells, 2).Count == 2);
        }

        private static Algorithm GetExampleAlgorithm(int k = 1)
        {
            var blocks = GetExampleBlocks();
            return new Algorithm(blocks, Enumerable.Repeat(1, blocks.Count).ToArray(), 10)
            {
                K = k
            };
        }

        private static List<Block> GetExampleBlocks()
        {
            int index = 0;
            return new List<Block>()
            {
                new Block(new[,] { { true,true,true,true } }, 4, index++),//I  "długi"
                new Block(new[,] { { true,true,true }, {false,false,true} }, 4, index++),//J
                new Block(new[,] { { true,true,true }, {true,false,false} }, 4, index++),//L
                new Block(new[,] { { true,true }, {true,true} }, 4, index++),//O
                new Block(new[,] { { false,true,true } , {true,true,false } }, 4, index++),//S
                new Block(new[,] { { true,true,true }, {false,true,false} }, 4, index++),//T
                new Block(new[,] { { true,true,false }, {false,true,true} }, 4, index++),//Z
            };
        }
    }
}
