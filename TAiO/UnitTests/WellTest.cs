﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris.Model;
using System.Collections.Generic;
using Tetris.Algorithm;

namespace UnitTests
{
    [TestClass]
    public class WellTest
    {
        [TestMethod]
        public void WellComparerAreEqualTest()
        {
            var blocks = GetExampleBlocks();
            var well = new Well(10, 10);
            for(int i = 0; i < 3; ++i)
            {
                well.AddBlock(blocks[i], i);
            }
            var copyWell = Helper.DeepCopy(well);
            var comparer = new WellComparer();
            Assert.IsTrue(comparer.Equals(well, copyWell));
        }
        [TestMethod]
        public void WellComparerNotEqualTest()
        {
            var blocks = GetExampleBlocks();
            var w = new Well(10, 10);
            var v = new Well(10, 10);
            int N = 3;
            for (int i = 0; i < N; ++i)
            {
                w.AddBlock(blocks[i], i);
                v.AddBlock(blocks[i + 1], i + 1);
            }
            var comparer = new WellComparer();
            Assert.IsFalse(comparer.Equals(w, v));
        }

        private static List<Block> GetExampleBlocks()
        {
            int index = 0;
            return new List<Block>()
            {
                new Block(new[,] { { true,true,true,true } }, 4, index++),//I  "długi"
                new Block(new[,] { { true,true,true }, {false,false,true} }, 4, index++),//J
                new Block(new[,] { { true,true,true }, {true,false,false} }, 4, index++),//L
                new Block(new[,] { { true,true }, {true,true} }, 4, index++),//O
                new Block(new[,] { { false,true,true } , {true,true,false } }, 4, index++),//S
                new Block(new[,] { { true,true,true }, {false,true,false} }, 4, index++),//T
                new Block(new[,] { { true,true,false }, {false,true,true} }, 4, index++),//Z
            };
        }
    }
}
