﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GUI.Converters;
using System.Globalization;

namespace UnitTests
{
    [TestClass]
    public class ConvertersTest
    {
        [TestMethod]
        public void InverseBooleanConverterTest()
        {
            var ibConverter = new InverseBooleanConverter();
            Assert.IsFalse((bool)ibConverter.Convert(true, typeof(bool), null, null));
            Assert.IsTrue((bool)ibConverter.Convert(false, typeof(bool), null, null));
        }
    }
}
