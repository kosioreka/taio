﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris.Model;

namespace UnitTests
{
    [TestClass]
    public class BlockEqualsTest
    {
        #region ARRANGE

        private bool[,] Tab1()
        {
            return new[,]
            {
                {true, false},
                {true, true},
                {false, true}
            };
        }

        private Block Block1()
        {
            return new Block(Tab1(), 4);
        }

        private bool[,] Tab2()
        {
            return new[,]
            {
                {true, false},
                {true, true}
            };
        }

        private Block Block2()
        {
            return new Block(Tab2(), 3);
        }

        //4 obrócony o 180st
        private bool[,] Tab3()
        {
            return new[,]
            {
                {true, true, true, true},
                {true, false, false, false},
                {true, false, false, false}
            };
        }

        private Block Block3()
        {
            return new Block(Tab3(), 6);
        }

        //3 obrócony o 180st
        private bool[,] Tab4()
        {
            return new[,]
            {
                {false, false, false, true},
                {false, false, false, true},
                {true, true, true, true}
            };
        }

        private Block Block4()
        {
            return new Block(Tab4(), 6);
        }


        //6 obrócony o 90st
        private bool[,] Tab5()
        {
            return new[,]
            {
                {false, false, false, true},
                {true, true, true, true}
            };
        }

        private Block Block5()
        {
            return new Block(Tab5(), 6);
        }

        //5 obrócony o 90st
        private bool[,] Tab6()
        {
            return new[,]
            {
                {true, false},
                {true, false},
                {true, false},
                {true, true}
            };
        }

        private Block Block6()
        {
            return new Block(Tab6(), 6);
        }    
        
        //symetryczny z 8
        private bool[,] Tab7()
        {
            return new[,]
            {
                {true, false, false, false, false},
                {true, false, false, false, false},
                {true, false, false, false, false},
                {true, true, true, true, true}
            };
        }

        private Block Block7()
        {
            return new Block(Tab7(), 8);
        }

        //symetryczny z 7
        private bool[,] Tab8()
        {
            return new[,]
            {
                {false, false, false, false, true},
                {false, false, false, false, true},
                {false, false, false, false, true},
                {true, true, true, true, true}
            };
        }
        
        private Block Block8()
        {
            return new Block(Tab8(), 8);
        }


        //obrócone odbicie 10
        private bool[,] Tab9()
        {
            return new[,]
            {
                {false, false, false, false, true},
                {false, false, false, false, true},
                {false, false, false, false, true},
                {true, true, true, true, true}
            };
        }

        private Block Block9()
        {
            return new Block(Tab9(), 9);
        }

        //obrócone odbicie 9
        private bool[,] Tab10()
        {
            return new[,]
            {
                {true, true, true, true},
                {true, false, false, false},
                {true, false, false, false},
                {true, false, false, false},
                {true, false, false, false}
            };
        }

        private Block Block10()
        {
            return new Block(Tab10(), 9);
        }
        #endregion

        //ten sam klocek
        [TestMethod]
        public void TestMethod1()
        {
            Block b1 = Block1();
            var b2 = b1;

            Assert.IsTrue(b1.Equals(b2));
        }

        //ta sama tablica
        [TestMethod]
        public void TestMethod2()
        {
            var tab = Tab1();
            var b1 = new Block(tab, 4);
            var b2 = new Block(tab, 4);

            Assert.IsTrue(b1.Equals(b2));
        }

        //identyczne klocki
        [TestMethod]
        public void TestMethod3()
        {
            var b1 = Block2();
            var b2 = Block2();

            Assert.IsTrue(b1.Equals(b2));
        }

        //identyczne tablice - 180st
        [TestMethod]
        public void TestMethod4()
        {
            var b3 = Block3();
            var b4 = Block4();

            Assert.IsTrue(b3.Equals(b4));
        }

        //identyczne tablice - 90st
        [TestMethod]
        public void TestMethod5()
        {
            var b5 = Block5();
            var b6 = Block6();

            Assert.IsTrue(b5.Equals(b6));
        }

        //różne tablice - odbicie symetryczne
        [TestMethod]
        public void TestMethod6()
        {
            var b7 = Block7();
            var b8 = Block8();

            Assert.IsFalse(b7.Equals(b8));
        }

        //różne tablice - odbicie symetryczne obrócone
        [TestMethod]
        public void TestMethod7()
        {
            var b9 = Block9();
            var b10 = Block10();

            Assert.IsFalse(b9.Equals(b10));
        }
    }
}
