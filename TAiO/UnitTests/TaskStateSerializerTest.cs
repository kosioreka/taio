﻿using System;
using System.Collections.Generic;
using System.IO;
using GUI.Logic;
using GUI.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris.Model;

namespace UnitTests
{
    [TestClass]
    public class TaskStateSerializerTest
    {
        [TestMethod]
        public void SerializationBeginsWithTask()
        {
            const string tetrisInput = @"4 7
4 1
1 1 1 1
3 2
1 1 1
0 0 1
3 2
1 1 1
1 0 0
2 2
1 1
1 1
3 2
0 1 1
1 1 0
3 2
1 1 1
0 1 0
3 2
1 1 0
0 1 1";
            var task = GetExampleTaioTask();
            var vm = new MainWindowVM(task);

            var memoryStream = new MemoryStream();
            var writer = new StreamWriter(memoryStream);

            //act
            StateSerializationLogic.SaveState(writer, vm);

            //assert
            memoryStream.Seek(0, SeekOrigin.Begin);

            var content = new StreamReader(memoryStream).ReadToEnd();
            //
            Console.WriteLine(content);
            //
            Assert.IsTrue(content.StartsWith(tetrisInput));
        }

        [TestMethod]
        public void SerializeBeforeStartTest()
        {
            var task = GetExampleTaioTask();
            var vm = new MainWindowVM(task);

            var memoryStream =new MemoryStream();
            var writer =new StreamWriter(memoryStream);

            //act
            StateSerializationLogic.SaveState(writer, vm);

            //assert
            memoryStream.Seek(0, SeekOrigin.Begin);

            var content = new StreamReader(memoryStream).ReadToEnd();
            //
            Console.WriteLine(content);
            //

            //TODO lepszy warunek
            Assert.IsFalse(string.IsNullOrWhiteSpace(content));
        }

        [TestMethod]
        public void SerializeAfterOneStepTest()
        {
            var task = GetExampleTaioTask();
            var vm = new MainWindowVM(task);
            vm.AlgorithmWrapper.NextStep();

            var memoryStream = new MemoryStream();
            var writer = new StreamWriter(memoryStream);

            //act
            StateSerializationLogic.SaveState(writer, vm);

            //assert
            memoryStream.Seek(0, SeekOrigin.Begin);

            var content = new StreamReader(memoryStream).ReadToEnd();
            //
            Console.WriteLine(content);
            //

            //TODO lepszy warunek
            Assert.IsFalse(string.IsNullOrWhiteSpace(content));
        }

        private static TaioTask GetExampleTaioTask()
        {
            var task = new TaioTask(4, new List<Block>
            {
                new Block(new[,] {{true, true, true, true}}, 4), //I  "długi"
                new Block(new[,] {{true, true, true}, {false, false, true}}, 4), //J
                new Block(new[,] {{true, true, true}, {true, false, false}}, 4), //L
                new Block(new[,] {{true, true}, {true, true}}, 4), //O
                new Block(new[,] {{false, true, true}, {true, true, false}}, 4), //S
                new Block(new[,] {{true, true, true}, {false, true, false}}, 4), //T
                new Block(new[,] {{true, true, false}, {false, true, true}}, 4), //Z
            });
            return task;
        }
    }
}
