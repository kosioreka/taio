﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris.Model;
using Tetris.Parser;

namespace UnitTests
{
    [TestClass]
    public class TaskParserTest
    {
        /// <summary>Sprawdza banalny przykład</summary>
        [TestMethod]
        public void TrivialTaskParserTest()
        {
            const string trivialInput = @"1 1
1 1
1";
            RunTest(trivialInput, new TaioTask(1, new List<Block> { new Block(new[,] { { true } }, 1) }));
        }

        /// <summary>Sprawdza przykład klasycznego Tetrisa
        /// Zaczerpnięto z https://en.wikipedia.org/wiki/Tetris </summary>
        [TestMethod]
        public void TetrisTaskParserTest()
        {
            const string tetrisInput = @"4 7
4 1
1 1 1 1
3 2
1 1 1
0 0 1
3 2
1 1 1
1 0 0
2 2
1 1
1 1
3 2
0 1 1
1 1 0
3 2
1 1 1
0 1 0
3 2
1 1 0
0 1 1";
            RunTest(tetrisInput, new TaioTask(4, new List<Block>
            {
                new Block(new[,] { { true,true,true,true } }, 4),//I  "długi"
                new Block(new[,] { { true,true,true }, {false,false,true} }, 4),//J
                new Block(new[,] { { true,true,true }, {true,false,false} }, 4),//L
                new Block(new[,] { { true,true }, {true,true} }, 4),//O
                new Block(new[,] { { false,true,true } , {true,true,false } }, 4),//S
                new Block(new[,] { { true,true,true }, {false,true,false} }, 4),//T
                new Block(new[,] { { true,true,false }, {false,true,true} }, 4),//Z
            }));
        }

        private void RunTest(string input, TaioTask expectedTask)
        {
            var task = ParseTask(SplitInputLines(input));

            Assert.IsTrue(TasksEqual(task, expectedTask));
        }

        private bool TasksEqual(TaioTask task, TaioTask expectedTask)
        {
            if (task == null || task.WellWidth != expectedTask.WellWidth) return false;
            if (task.AvailableBlocks.Count != expectedTask.AvailableBlocks.Count) return false;
            return !task.AvailableBlocks.Where((t, i) => !BlocksEqual(t, expectedTask.AvailableBlocks[i])).Any();
        }

        private bool BlocksEqual(Block block, Block expectedBlock)
        {
            if (block.Width != expectedBlock.Width || block.Height != expectedBlock.Height) return false;
            for (var i = 0; i < block.Height; i++)
            {
                for (var j = 0; j < block.Width; j++)
                {
                    if (block.OccupiedSpace[i, j] != expectedBlock.OccupiedSpace[i, j]) return false;
                }
            }
            return true;
        }

        private static TaioTask ParseTask(string[] input)
        {
            return new TaskParser().ParseLines(input);
        }

        private static string[] SplitInputLines(string input)
        {
            return input.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        }
    }
}
