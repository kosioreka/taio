﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GUI.ViewModel;
using Tetris.Model;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class ResetAlgorithmTest
    {
        [TestMethod]
        public void ResetTest()
        {
            var task = GetExampleTaioTask();
            var vm = new MainWindowVM(task);
            vm.AlgorithmWrapper.NextStep();
            Assert.IsTrue(vm.AlgorithmWrapper.NumberOfBlocksAdded > 0
                && vm.AlgorithmWrapper.StepsDone > 0);
            vm.ResetTask();
            Assert.IsTrue(vm.AlgorithmWrapper.NumberOfBlocksAdded == 0
                && vm.AlgorithmWrapper.StepsDone == 0);
        }
        [TestMethod]
        public void ResetByKChangeTest()
        {
            var task = GetExampleTaioTask();
            var vm = new MainWindowVM(task);
            vm.AlgorithmWrapper.NextStep();
            Assert.IsTrue(vm.AlgorithmWrapper.NumberOfBlocksAdded > 0
                && vm.AlgorithmWrapper.StepsDone > 0);
            vm.K++;
            Assert.IsTrue(vm.AlgorithmWrapper.NumberOfBlocksAdded == 0
                && vm.AlgorithmWrapper.StepsDone == 0);
        }
        [TestMethod]
        public void ResetByTaskChangeTest()
        {
            var task = GetExampleTaioTask();
            var vm = new MainWindowVM(task);
            vm.AlgorithmWrapper.NextStep();
            Assert.IsTrue(vm.AlgorithmWrapper.NumberOfBlocksAdded > 0
                && vm.AlgorithmWrapper.StepsDone > 0);
            vm.SetNewTaioTask(task);
            Assert.IsTrue(vm.AlgorithmWrapper.NumberOfBlocksAdded == 0
                && vm.AlgorithmWrapper.StepsDone == 0);
        }

        private static TaioTask GetExampleTaioTask()
        {
            var task = new TaioTask(4, new List<Block>
            {
                new Block(new[,] {{true, true, true, true}}, 4), //I  "długi"
                new Block(new[,] {{true, true, true}, {false, false, true}}, 4), //J
                new Block(new[,] {{true, true, true}, {true, false, false}}, 4), //L
                new Block(new[,] {{true, true}, {true, true}}, 4), //O
                new Block(new[,] {{false, true, true}, {true, true, false}}, 4), //S
                new Block(new[,] {{true, true, true}, {false, true, false}}, 4), //T
                new Block(new[,] {{true, true, false}, {false, true, true}}, 4), //Z
            });
            return task;
        }
    }
}
